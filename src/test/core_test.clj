 (ns test.core-test
  (:require [clojure.test :as t]))

(defn adjacentElementProductMax [arr]
  (->> arr
     (partition 2 1)
     (map #(apply * %))
     (reduce max)))

(t/deftest case-tests
  (let [case1 [3, 6, -2, -5, 7, 3]
        case2 [2 1 -328674 2184632178 3.14315 28]]
    (t/is (= 21 (adjacentElementProductMax case1)))
    (t/is (= 6.8666266302807E9 (adjacentElementProductMax case2)))))
