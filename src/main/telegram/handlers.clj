(ns main.telegram.handlers
  (:require [main.config :refer (config)]
            [clojure.string :as string]))

(def telegram-token (get (config :test) :telegram-token))

(defn handle-message
  [{{:keys [id]} :chat
    :keys        [text message_id]}]
  (when (string/starts-with? text "/get ")
    (let [hash (string/trim (subs text 4))]
      {:em-respota {:reply_to_message_id message_id
                    :chat_id             id}
       :request    {:method :get
                    :url    (format "http://localhost:5002/ipfs/%s"
                                    hash)}})))

(defn handle-get-ipfs
  [{{:keys [body]}                        :response
    {:keys [reply_to_message_id chat_id]} :em-respota}]
  {:method      :post
   :as          :json
   :url         (format "https://api.telegram.org/bot%s/sendMessage"
                        telegram-token)
   :form-params {:reply_to_message_id reply_to_message_id
                 :chat_id             chat_id
                 :text                body}})
